var mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;
var http = require("http");
var async = require('async');

var options = {
  server: { poolSize: 5 }
}

mongoose.connect(process.env.MONGODB_URL, options);
var serviceSchema = mongoose.Schema({
	name: String,
	url: String,
	comment: String,
	status: Number,
	owner: String ,
	uptime: [Number],
	uptime_last: Number,
	updated: {
		type: Date,
		default: Date.now
	}
});
var Service = mongoose.model('service', serviceSchema);


exports.index = function(req, res){
	return Service.find().sort('name').exec(function(err, docs){
		res.render('index', { items: docs });
	});
};

exports.edit = function(req, res){
	return Service.findOne( {_id : new ObjectId(req.params.id)} , function(err, post){
		res.render('edit', {mode: "編集", item: post, id: req.params.id});
	});
};

exports.delete = function(req, res){
	return Service.findOne( {_id : new ObjectId(req.params.id)} , function(err, post){
		res.render('delete', {item: post, id: req.params.id});
	});
};

exports.delete_post = function(req, res){
	return Service.remove({_id : new ObjectId(req.body._id)}, function(err){
		res.redirect('/');
	});
};


//integration for Kato
function Notify(msg, color){

	if(process.env.KATO_ROOM_URL){
		var request = require('request');
		var options = {
		  url: process.env.KATO_ROOM_URL,
		  headers: {  'Content-Type': 'application/json' },
		  json: true,
		  body: JSON.stringify({ 
		    "from": "IPL-WS",
		    "color": color,
		    "renderer": "markdown",
		    "text": msg
		  })
		};

		request.post(options, function(error, response, body){
		  if (!error && response.statusCode == 200) {
		    console.log(body.name);
		  } else {
		    console.log('error: '+ response.statusCode);
		  }
		});
	}
}


function updateDoc(doc, next){
	//console.dir(doc);
	if(doc.url !== ""){
		var nowTime = new Date().getTime();
		console.log("Fetching " + doc.url + "");
		var httpget = http.get(doc.url, function(res){
			var diffTime = new Date().getTime() - nowTime;
			console.log("  " + doc.url + " ... " + res.statusCode + " in " + diffTime );
			if(!doc.uptime || doc.uptime.length == 0){
				doc.uptime = [diffTime];
			}else{
				doc.uptime.push(diffTime);
				if(doc.uptime.length > 10){
					doc.uptime.shift();
				}
			}
			doc.uptime_last = diffTime;
			//when the status changed
			//if( doc.status !== res.statusCode){
/*
			    switch(String(res.statusCode).charAt(0)){
			        case "0" : Notify(doc.name + "が異常です。 [参照](http://192.168.5.56:3000/)", "gray");break;
			        case "9" : Notify(doc.name + "が応答しません。 [参照](http://192.168.5.56:3000/)", "gray");break;
			        case "2" : Notify(doc.name + "が正常稼動しました。 [参照](http://192.168.5.56:3000/)", "green");break;
			        case "3" : Notify(doc.name + "が正常稼動しました。 [参照](http://192.168.5.56:3000/)", "green");break;
			        case "4" : Notify(doc.name + "が応答しません。 [参照](http://192.168.5.56:3000/) status: " + res.statusCode, "red");break;
			        case "5" : Notify(doc.name + "が異常です。 [参照](http://192.168.5.56:3000/) status: " + res.statusCode, "red");break;
			    }
*/				
				doc.status = res.statusCode;
				return doc.save(function(){
					console.log("saved");
					httpget.abort();
					next();
				});
			//}else{
				//httpget.abort();
			//}
			//next();
			
		}).on("error", function(e){
			if(e.message !== "read ECONNRESET"){
				console.log(doc.url + " ... " + e.message);
				doc.status = 0;
				return doc.save(function(){
					httpget.abort();
					next();
				});			
			}
			//Should I notify? It will occur every minute.
		});
		httpget.setTimeout(10000, function(){
			//console.log("Start")
			doc.status = 9000;
			console.log("timeout: " + doc.url);
			return doc.save(function(){
					httpget.abort();
					next();
			});
			
		})
		httpget.shouldKeepAlive = false;
		return httpget;
	}
}

exports.updateStatus = function(req, res){
	res.json({result : 'success'});
	return Service.find({}, function(err, docs){
		//for(var i in docs){
		async.waterfall([
			function(){
				async.eachSeries(docs, function(item, next){
					updateDoc(item, next);
				});
			}, function(){
			}
		]);
	});
};

exports.add = function(req, res){
	var post = new Service();
	res.render('edit', {mode: "新規追加", item: post});
};

exports.edit_post = function(req,res){
	var p_id = req.body._id;
	var p_name = req.body.name;
	var p_url = req.body.url;
	var p_comment = req.body.comment;
	var p_status = req.body.status;
	var p_owner = req.body.owner;

	if(p_id === ""){
		var s = new Service();
		s.name = p_name;
		s.url = p_url;
		s.comment = p_comment;
		s.status = p_status;
		s.owner = p_owner;
		return s.save(
			function(err){
				res.redirect('/');
			}
		);
	}else{
		return Service.findOne( {_id : new ObjectId(p_id)} , function(err, s){
			s.name = p_name;
			s.url = p_url;
			s.comment = p_comment;
			s.status = p_status;
			s.owner = p_owner;			
			return s.save(
				function(err){
					res.redirect('/');
				}
			);

		});
	}


}
